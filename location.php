<?php include "inc/head.php";
?>
<div class="hero sub" style="background-image:url('<?=$pages->get('/leasing/')->bg_img->url?>');">
</div>
<div id="top" class="content-wrapper">
	<div class="content">
		<div class="title-container">
			<h2>Properties located in <strong><?=$page->title?></strong></h2>
			<p>
				<a href="<?=$page->parent->url?>">VIEW OTHER LOCATIONS »</a>
			</p>
		</div><!--title-->

<?php
$properties = $pages->find("location=".$page->id);
if(count($properties)){?>
	<div id="propertyGrid" class="gallery">
	<?php
		$resultsNumber = count($properties);?>
		<div id="resultsAmount"><?=$resultsNumber?></div>
		<?php $i=1;
		foreach($properties as $prop){
			$thumb = $prop->images->first()->size(300,150);	?>
			
			<div class="prop-small <?=$prop->property_type->implode(' ', 'name')?>" id="<?=$prop->name?>" data-slide="<?=$i?>">
			<div class="more-info">
				<img class="thumb" src="<?=$thumb->url?>">
				<h3><?=$prop->title?></h3>
				<p><?=$prop->location->title?></p>
				<p><?=$prop->rentable_area?></p>
			</div><!--info-->
			</div><!--slide-->
			<?php $i++;
			}
			
		?>
	</div><!--grid-->
	<div class="popup leasing-box">
		<div class="exit-wrapper">
			<svg id="exitFrame" class="exit"><use xlink:href="#popup-exit"></use></svg>
		</div><!--exit-wrapper-->
		<div id="propertyExpand" class="large">
			<?php foreach($properties as $prop){ ?>
			<div class="content <?=$prop->property_type->implode(' ', 'name')?>" style="background-image:url('<?=$prop->images->first->url?>');">
				<div class="info">
					<div class="deets-wrapper">
						<p class="show">Show Details</p>
						<svg class="show-deets"><use xlink:href="#show-more"></use></svg>
					</div><!--wrapper-->
					<div class="details">
						<h2><?=$prop->title?></h2>
						<div class="portfolio-info">
							
							<ul class="quick-info">
								<li>Address: <?=$prop->address?></li>
								<li>City: <?=$prop->location->title?></li>
								<li>Type: <?=$prop->property_type->implode(", ", "title");?> </li>
								<li>NRA: <?=$prop->rentable_area?> </li>
							</ul>
							<p><?=$prop->body?></p>
							<ul class="gal">
							<?php foreach($prop->images as $img){
								$thumb = $img->size(200,150); ?>
								<li><a class="fancybox" data-fancybox="gallery-<?=$prop->id?>" href="<?=$img->url?>" data-img="<?=$img->url?>"><img src="<?=$thumb->url?>" alt="<?=$prop->title?> <?=$img->description?>" /></a></li> 
							<?php }?>
							</ul>
							<div class="leasing-only">
								<?php if($prop->link){?><p><strong><a href="<?=$prop->link?>" title="Open in Google Maps" target="_blank">VIEW LOCATION ON MAP</a></strong></p><?php }?>
								<?php if($prop->file){?><p><strong><a href="<?=$prop->file->url?>" title="View Brochure" target="_blank">VIEW BROCHURE</a></strong></p><?php }?>
								<h3>Contact</h3>
								<?=$prop->contact?>
								<?php if($prop->website){?>
								<p><strong><a target="_blank" href="<?=$prop->website?>" title="<?=$prop->title?> Website">VISIT WEBSITE</a></strong></p>
								<?php }?>
							</div><!--leasing only-->
						</div><!--portfolio-->
						<div class="track-info">
							<?php if($prop->track_record_info){ echo $prop->track_record_info; }?>
						</div><!--track-record-info-->
						

					</div><!--details-->
				</div><!--info-->
			</div><!--content-->
			<?php } ?>
		</div><!--propertyExpand-->
		
		<div id="propertyChooser" class="gallery">
			<?php foreach($properties as $prop){ ?>
			<div class="slide <?=$prop->property_type->implode(' ', 'name')?>" data-img="<?=$prop->images->first->url?>">
				<div class="info">
					<img class="thumb" src="<?=$prop->images->first()->size(300,150)->url?>">
					<h3><?=$prop->title?></h3>
					<p><?=$prop->location->title?></p>
					<p><?=$prop->rentable_area?></p>
				</div><!--info-->
			</div><!--slide-->
			<?php } ?>
		</div><!--propertyChooser-->
	</div><!--popup-->
	<?php } ?>
	</div><!--content-->
</div><!--wrapper-->

<?php
include 'inc/foot.php'; 
?>
