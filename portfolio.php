<?php 
include 'inc/head.php'; 
$properties = $pages->find("template=property, sort=sort, property_category.value=".$page->name."");
$types = $pages->get('/property-types/')->children;
?>
<script>
	var propURL = "<?=$pages->get('/properties/')->url;?>";
</script>
<div class="hero sub" style="background-image:url('<?=$page->bg_img->url?>');"></div>
<div id="top" class="content-wrapper">
	<div class="content">
		<div class="title-container">
			<h2><?=$page->title?></h2>
			<?=$page->body?>
		
		<?php 
		if($page->name == $leasing->name){?>
			<div class="search-categories">
			
				<a class="current" href="<?=$pages->get('/leasing/')->url?>" title="Leasing - All Categories">All</a>
				<?php
				foreach($types as $type){
					echo '<a href="'.$pages->get('/search/')->url.'?property_type='.$type->name.'&submit=Search">'.$type->title.'</a> ';
					
				}
				?>
		
			</div><!--search categories-->
		
		<?php } ?>	
		</div><!--title-container-->
		<div id="propertyGrid" class="gallery">

		<?php 
		$resultsNumber = $pages->count("template=property, property_category.value=".$page->name."");
		//this determines how many slides show up in #propertyCHooser ?>
		<div id="resultsAmount"><?=$resultsNumber?></div>
		<?php 
		$i=1;
		foreach($properties as $prop){
			$thumb = $prop->images->first()->size(300,150);	?>
			<div class="prop-small <?=$prop->property_type->implode(' ', 'name')?>" id="<?=$prop->name?>" data-slide="<?=$i?>">
				<div class="more-info">
					<img class="thumb" src="<?=$thumb->url?>">
					<h3><?=$prop->title?></h3>
					<p><?=$prop->location->title?></p>
					<?php if($page->name != 'track-record'){ if($prop->rentable_area){ echo '<p>NRA: '. $prop->rentable_area .'</p>';} }?>
				</div><!--info-->
			</div><!--slide-->
			<?php 	
			
			$i++;
		} ?>	
		</div><!--gallery-->

		<div class="popup <?=$page->name?>-box">
			<div class="exit-wrapper">
				<svg id="exitFrame" class="exit"><use xlink:href="#popup-exit"></use></svg>
			</div><!--exit-wrapper-->
			<div id="propertyExpand" class="large">
				<?php 
	
				foreach($properties as $prop){ ?>
				<div class="content <?=$prop->property_type->implode(' ', 'name')?>" style="background-image:url('<?=$prop->images->first->url?>');">

					<div class="info">
						<div class="deets-wrapper">
							<p class="show">Show Details</p>
							<svg class="show-deets"><use xlink:href="#show-more"></use></svg>
						</div><!--wrapper-->
						<div class="details">
							<h2><?=$prop->title?></h2>
							<div class="portfolio-info">
								
								<ul class="quick-info">
									<li><strong>Address:</strong> <?=$prop->address?> <?=$prop->location->title?></li>
									<li><strong>Type:</strong> <?=$prop->property_type->implode(", ", "title");?> </li>
									<li><strong>NRA:</strong> <?=$prop->rentable_area?> </li>
								</ul>
								<?php if($prop->website){?> <p><strong><a target="_blank" href="<?=$prop->website?>" title="<?=$prop->title?> Website"><svg class="icon"><use xlink:href="#icon-web"></use></svg> VISIT WEBSITE</a></strong></p><?php }?>
								<p><?=$prop->body?></p>
								<ul class="gal">
								<?php 
								foreach($prop->images as $img){
									$thumb = $img->size(200,150); ?>
									<li><a class="fancybox" data-fancybox="gallery-<?=$prop->id?>" href="<?=$img->url?>" data-img="<?=$img->url?>"><img src="<?=$thumb->url?>" alt="<?=$prop->title?> <?=$img->description?>" /></a></li> 
								<?php  
								}?>
								</ul>
								<div class="leasing-only">
									<div class="links-box">
										<?php if($prop->link){?><p><strong><a href="<?=$prop->link?>" title="Open in Google Maps" target="_blank"><svg class="icon"><use xlink:href="#icon-map"></use></svg> VIEW LOCATION ON MAP</a></strong></p><?php }?>
										<?php if($prop->file){?><p><strong><a href="<?=$prop->file->url?>" title="View Brochure" target="_blank"><svg class="icon"><use xlink:href="#icon-brochure"></use></svg> VIEW BROCHURE</a></strong></p><?php }?>
										<input class="copy-url" type="text" value="<?=$prop->httpUrl?>" id="<?=$prop->id?>-url" />
										<p><strong><a class="copy" href="#" data-url="<?=$prop->id?>-url"><svg class="icon"><use xlink:href="#icon-share"></use></svg> COPY URL</a></strong></p>
										
										

									</div><!--links-->
									
									
								</div><!--leasing only-->
								
								<?php if($prop->contact){?>
								<div class="contact-box">
								<h3>Contact</h3>
								<?=$prop->contact?>
								</div><!--contact box-->
								<?php }?>
									
							</div><!--portfolio-->
							<div class="track-info">
								<!--<?php if($prop->track_record_info){ echo $prop->track_record_info; }?>-->
								<p><strong>Purchase Date:</strong> <?=$prop->purchase_date?><?php if($prop->exit_date){?> <span>|</span> <strong>Exit Date:</strong> <?=$prop->exit_date?> <?php } if($prop->net_irr){?><span>|</span> <strong>Net IRR:</strong> <?=$prop->net_irr?><?php }?></p>
							</div><!--track-record-info-->
							
	
						</div><!--details-->
					</div><!--info-->
				</div><!--content-->
			<?php 
			}?>
			</div><!--propertyExpand-->
	
			<div id="propertyChooser" class="gallery">
				<?php foreach($properties as $prop){ ?>
				<div class="slide <?=$prop->property_type->implode(' ', 'name')?>" data-img="<?=$prop->images->first->url?>">
					<div class="info">
						<img class="thumb" src="<?=$prop->images->first()->size(300,150)->url?>">
						<h3><?=$prop->title?></h3>
						<p><?=$prop->location->title?></p>
						<p><?=$prop->rentable_area?></p>
					</div><!--info-->
				</div><!--slide-->
				<?php  }?>
			</div><!--propertyChooser-->
		</div><!--popup-->





		
		
		

		
	</div><!--content-->
	
</div> <!--wrapper-->



<?php include 'inc/foot.php'; ?>