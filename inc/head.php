<?php 

$home = $pages->get('/');
$about = $pages->get('/about/');
$portfolio = $pages->get('/portfolio/');
$team = $pages->get('/team/');
$track = $pages->get('/track-record/');
$leasing = $pages->get('/leasing/');
$mgmt = $pages->get('/property-management/');
$search = $pages->get('template=search');
$ogImage = count($page->images) ? $page->images->first()->size(1200,630)->httpUrl : $config->urls->httpTemplates."dest/img/facebook-og-01.jpg";

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Enright Capital Partners | <?=$page->title?></title>
	<meta name="description" content="<?php if($page->summary){echo $page->summary;} else {echo strip_tags($page->body);} ?>" />
	<link rel="author" href="<?=$config->urls->templates?>humans.txt" />
	<link rel="apple-touch-icon" sizes="180x180" href="<?=$config->urls->templates?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?=$config->urls->templates?>/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?=$config->urls->templates?>/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?=$config->urls->templates?>/manifest.json">
	<link rel="mask-icon" href="<?=$config->urls->templates?>/safari-pinned-tab.svg" color="#c41230">
	<meta name="theme-color" content="#ffffff">

	<meta property="og:url" content="<?=$page->httpUrl?>" >
	<meta property="og:type" content="website">
	<meta property="og:title" content="Enright Capital Partners | <?=$page->title?>">
	<meta property="og:description" content="<?php if($page->summary){echo $page->summary;} else {echo strip_tags($page->body);} ?>" />
	<meta property="og:image" content="<?=$ogImage?>">
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="630" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:title" content="Enright Capital Partners | <?=$page->title?>" />
	<meta name="twitter:description" content="<?php if($page->summary){echo $page->summary;} else {echo strip_tags($page->body);} ?>" />
	<meta property="twitter:image" content="<?=$ogImage?>" />
	<link async rel="stylesheet" type="text/css" href="<?=$config->urls->templates?>dest/styles/styles.min.css" />
</head>
<?php include 'inc/tracking.php';?>
<body class="<?=$page->template?>">
	<?php include 'svg/all.svg';?>
	<div class="container">
	<header>
		<a href="<?=$home->url?>"><svg class="logo"><use xlink:href="#enright-logo"></use></svg></a>
		<nav>
			<input id="mobileMenu" name="mobileMenu" type="checkbox">
			<label for="mobileMenu"><svg><use xlink:href="#icon-menu"></use></svg></label>
			
			<ul>
			
				<li><a <?php if($page->name == $about->name){echo "class='curr'"; }?> href="<?=$about->url?>">About</a></li>
				<li><a <?php if($page->name == $portfolio->name){echo "class='curr'"; }?> href="<?=$portfolio->url?>">Portfolio</a></li>
				<li><a <?php if($page->name == $team->name){echo "class='curr'"; }?> href="<?=$team->url?>">Team</a></li>
				<li><a <?php if($page->name == $leasing->name || $page->name == $search->name){echo "class='curr'"; }?> href="<?=$leasing->url?>">Leasing</a></li>
				<li><a <?php if($page->name == $track->name){echo "class='curr'"; }?> href="<?=$track->url?>">Track Record</a></li>
				<li><a <?php if($page->name == $mgmt->name){echo "class='curr'"; }?> href="<?=$mgmt->url?>">Property Management</a></li>
			</ul>
		</nav>
		
	</header>
	
	
		
		