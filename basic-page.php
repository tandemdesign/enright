<?php 
include 'inc/head.php'; 
?>

<div class="hero sub" style="background-image:url('<?=$page->bg_img->url?>');">
</div>

<div class="about content-wrapper">
	<div class="content">
		<div class="col-wrapper">
			<div class="col two">
				<h2><?=$page->title?></h2>
				<?=$page->body?>
			</div><!--col-->
			<div class="col two borderL">
				<?=$page->sidebar?>
			</div><!--col-->
		</div><!--col-wrapper-->
	</div><!--content-->
	


<?php include 'inc/foot.php'; ?>