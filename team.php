<?php 
include 'inc/head.php'; 
?>

<div class="hero sub" style="background-image:url('<?=$page->bg_img->url?>');">
</div>

<div class="content-wrapper">
	<div class="content">
		<div class="title-container">
			<h2>Principals</h2>
			<?=$page->body?>
		</div><!--title container-->
		
		<?php foreach($page->team_partners as $partner){?>
		<div class="profile-wrapper">
			<img src="<?=$partner->images->first->url?>" alt="<?=$partner->title?> of Enright Capital Partners" />
			<div class="bio">
				<h3><?=$partner->title?></h3>
				<?=$partner->body?>

			</div><!--bio-->
		</div><!--profile-->
		<?php }?>

		<div class="title-container">
			<h2>Organization</h2>
		</div><!--title-container-->

			<?php foreach($page->team_category as $category){?>
			<h3><?=$category->title?></h3>
			<div class="team-grid<?php if($category == $page->team_category->first){ echo ' first';}?>">
				<?php foreach($category->team_member as $member){?>
				<div class="grid-item">
					<p><strong><?=$member->title?></strong><br />
					<?=$member->headline?><br />
					<a href="mailto:<?=$member->email?>" /><?=$member->email?></a></p>
					<?=$member->body?>
				</div><!--grid-item-->
				<?php }?>
			</div><!--team-grid-->
			<?php }?>	
	</div><!--content-->
	
</div> <!--wrapper-->


<?php include 'inc/foot.php'; ?>