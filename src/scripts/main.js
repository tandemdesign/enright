//main.js

$(document).ready(function(){

	$('.img-wrapper').click(function(){
		$(this).toggleClass('big');
	});

	$('.about-link').click(function(e){
		e.preventDefault();
		//console.log('clicked');
		$(this).toggleClass('curr');
		$('#about').toggleClass('active');
	});
	$('#heroSlider').slick({
		autoplay: true,
		autoplaySpeed: 6000,
		arrows: false,
		dots: false
	});

	
	//make clicking show-deets work even when slider is re-opened
	//have 2 click events basically doing the same thing becuase one doesn't work in firefox and the other doesn't work in safari, chrome, etc and I honestly just don't want to deal with it/make it look nice
	$(document).on('click', 'svg.show-deets', function(){
		$(event.target).toggleClass('flip');
		$('.deets-wrapper').next().toggleClass('showing');
		$('.large .info').toggleClass('bg');
		if($(event.target).hasClass('flip')){
			$('p.show').text("Hide Details");
		} else{
			$('p.show').text("Show Details");
		}

		return false;
	});
	$('svg.show-deets').click(function(){
		$(this).toggleClass('flip');
		$('.deets-wrapper').next().toggleClass('showing');
		$('.large .info').toggleClass('bg');
		if($(this).hasClass('flip')){
			$('p.show').text("Hide Details");
		} else{
			$('p.show').text("Show Details");
		}

		return false;
	});
	
	var results = $('#resultsAmount').html();
	var slidesAmount;
	var isCenter = false;
	//console.log($.isNumeric(results));
	//onsole.log(results);
	if(results > 5){
		slidesAmount = 5;
	} else if (results <= 5 && results > 3){
		slidesAmount = 3;
	} else {
		slidesAmount = 1;
		isCenter = true;
	}
	//console.log(slidesAmount);
	
	// this opens the portfolio pop up when property is clicked
	$('#propertyGrid .prop-small').click(function(){
		var thisID = $(this).attr('id');
		var thisSlide = $(this).attr('data-slide');
		var aTag = $('#top');
		$('html,body').animate({scrollTop: aTag.offset().top}, 'slow');
		
		$('.popup').addClass('active');
		$('#exitFrame').addClass('active');

		//initializes sliders
		$('#propertyExpand').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			lazyLoad: 'ondemand',
			asNavFor: '#propertyChooser',
			
		});
		$('#propertyChooser').slick({
			slidesToScoll:1,
			slidesToShow:slidesAmount,
			asNavFor: '#propertyExpand',
			focusOnSelect: true,
			//centerMode: isCenter,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow:3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 600,
					settings: {
   	  			slidesToShow: 1,
   	  			slidesToScroll: 1
   	  		}
				}
			]
		});
		$('#propertyExpand').slick('setPosition');
		$('#propertyChooser').slick('slickGoTo', thisSlide - 1);
		$('#propertyGrid').addClass('expanded');


		
		
	});
	
	
	
	// closes popup
	$('#exitFrame').click(function(){
		$('#propertyGrid').removeClass('expanded');
		$('.popup').removeClass('active');
		//$('.popup .content >').remove();
		//$('.content-wrapper .content').removeClass('hidden');
		$('#propertyExpand').removeClass('active');
		$('#propertyExpand').slick('unslick');
		$('#propertyChooser').slick('unslick');
		$(this).removeClass('active');
		$('.show-deets').removeClass('flip');
		$('.large .info').removeClass('bg');
		$('.deets-wrapper').next().removeClass('showing');
		$('p.show').text("Show Details");
		$('a.copy').removeClass('copied');
		$('a.copy').html('<svg class="icon"><use xlink:href="#icon-share"></use></svg> COPY URL');
		//$('footer').show();
	});
	


	
	//click thumbnail to change bg img
	
	//$(document).on('click', 'ul.gal li a', changeBg);

	function changeBg(e){
		e.preventDefault();
		var imgURL = $(e.currentTarget).attr('data-img');
		//console.log($(e.currentTarget).attr('data-img'));
		//console.log(imgURL);
		$('#propertyExpand .content').css('background-image', 'url('+imgURL+')');
	}
	
	$('#propertyChooser').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		//console.log($(slick.$slides.get(currentSlide)).attr('data-img'));
		//console.log($("[data-slick-index='" +currentSlide+ "'] .slide").attr('data-img'));
		
		//resets everything on slide change
		var imgURL = $("[data-slick-index='" +nextSlide+ "'] .slide").attr('data-img');
		console.log(imgURL);
		$('#propertyExpand .content').css('background-image', 'url('+imgURL+')');
		$('.show-deets').removeClass('flip');
		$('.large .info').removeClass('bg');
		$('.deets-wrapper').next().removeClass('showing');
		$('p.show').text("Show Details");
		//$(document).on('click', 'ul.gal li a', changeBg);
		
 	});
 	$('a.copy').click(function(e){
 		e.preventDefault();
 		var thisURL = $(this).attr('data-url');
 		//console.log(thisURL);
 		copyURL(thisURL);
 		$(this).addClass('copied');
 		$(this).html('<svg class="icon"><use xlink:href="#icon-share"></use></svg> COPIED!');
 	});
	function copyURL(url){
		var text = $('#'+url);
		text.select();
		document.execCommand('copy');
		console.log('Copied');
	}
	

	
});