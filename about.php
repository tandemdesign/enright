<?php 
include 'inc/head.php'; 
?>

<div class="hero sub" style="background-image:url('<?=$page->bg_img->url?>');">
</div>

<div class="about content-wrapper">
	<div class="content">
		<div class="col-wrapper">
			<div class="col two">
				<h2>About Us</h2>
				<?=$page->body?>
			</div><!--col-->
			<div class="col two borderL">
					<?=$page->sidebar?>
					
					<!--
					<div class="col two">
						<img src="<?=$config->urls->templates?>dest/img/icon-development.png" /> <h2>Development</h2>
						<p>Approximately 800,000 sq. ft. and $215,000,000 of ongoing or completed development projects over the past 5 years.</p>
					</div>-->
				
			</div><!--col-->
		</div><!--col-wrapper-->
	</div><!--content-->
	


<?php include 'inc/foot.php'; ?>