<?php 
include 'inc/head.php'; 
?>
<div class="shadow"></div>
<div id="heroSlider" class="hero slider">
<?php foreach($page->images as $img){?>
<div class="slide" style="background-image:url('<?=$img->url?>');">
	<div class="content">
	</div>
</div>
<?php }?>
	
	
</div>


<div class="content-wrapper property-single">
	<div class="content">
		<div class="title-container">
			<h2><?=$page->title?></h2>
			
		</div>
		<div class="details">
			<div>
				<p><strong>Address:</strong> <?=$page->address?><br />
				<strong>City:</strong> <?=$page->location->title?><br />
				<strong>Type:</strong> <?=$page->property_type->implode(", ", "title");?> <br />
				<strong>NRA:</strong> <?=$page->rentable_area?> </p>
				<h3>Summary</h3>
				<?=$page->body?>
			</div>
			<div>
				<ul class="gal">
					<?php 
					foreach($page->images as $img){
						$thumb = $img->size(200,150); ?>
						<li><a class="fancybox" data-fancybox="gallery-<?=$page->id?>" href="<?=$img->url?>" data-img="<?=$img->url?>"><img src="<?=$thumb->url?>" alt="<?=$page->title?> <?=$img->description?>" /></a></li> 
					<?php }?>
				</ul>
				<?php if($page->link){?><p><strong><a href="<?=$page->link?>" title="Open in Google Maps" target="_blank"><svg class="icon"><use xlink:href="#icon-map"></use></svg> VIEW LOCATION ON MAP</a></strong></p><?php }?>
				<?php if($page->file){?><p><strong><a href="<?=$page->file->url?>" title="View Brochure" target="_blank"><svg class="icon"><use xlink:href="#icon-brochure"></use></svg> VIEW BROCHURE</a></strong></p><?php }?>
				<?php if($page->website){?> <p><strong><a target="_blank" href="<?=$page->website?>" title="<?=$page->title?> Website"><svg class="icon"><use xlink:href="#icon-web"></use></svg> VISIT WEBSITE</a></strong></p><?php }?>
				<input class="copy-url" type="text" value="<?=$page->httpUrl?>" id="<?=$page->id?>-url" />
				<p><strong><a class="copy" href="#" data-url="<?=$page->id?>-url"><svg class="icon"><use xlink:href="#icon-share"></use></svg> COPY URL</a></strong></p>
				
				<h3>Contact</h3>
				<?=$page->contact?>
			</div>
		</div><!--info-->
		<p class="more"><strong><a href="<?=$leasing->url?>">« VIEW OTHER PROPERTIES FOR LEASE</a></strong></p>
	</div><!--content-->
	
	

<?php include 'inc/foot.php'; ?>