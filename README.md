# Enright Capital - Templates Folder

[Processwire CMS](https://processwire.com/)

Uses [direct output](https://processwire.com/docs/front-end/output/direct/)

**Testing URL:** /sitetesting/enright/

**To Log In:** /processwire

Clients are able to update projects and some copy. 

**Issues:** Some issues are occuring with file uploads (on live site only, so this is a server issue). Sometimes have to go into database and manually add files. Server probably needs to be updated to PHP 7 but it's on Telus so prob not gonna happen. 

_I was hoping to get around to moving this website to our own server._


## Compile steps 

Styles: 

`$ gulp styles`

JS:

`$ gulp scripts`

Images:

`$ gulp images`

Watch: 

`$ gulp watch`

**Source Directory:** /src

**Dest Directories:** /dest

## Template info

- **portforlio:** Used for Project Portfolio types: Leasing & Sales, Track Record and Portfolio. Only shows projects that are in that category via a check box in CMS. 
- **property:** Used for Properties. Has title, city, address, property type, rentable area, portfolio type (leasing, track record, portfolio), body copy, image gallery, leasing info (when in Leasing category), track record info (when in Track Record category), Brochure upload and website link, 
- **category:** Parent page for Properties, Location, Property Types (Retail, Office, etc).
- **location:** Location category page field for categorizing Properties. User is able to choose or create a new location in a Property's edit panel. 
- **type:** Property Type category page field for categorizing Properties. User is able to choose or create a new Property Type in a Property's edit panel.
- **download:** Not an actual page, used by client to upload secure files.
- **home:** Home page. Slider is repeater. 
- **search:** Searches properties by property type.
- **team:** Team page. 
  

